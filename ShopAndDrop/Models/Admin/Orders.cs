﻿using ShopAndDrop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopAndDrop.Models.Admin
{
    public class Orders
    {
        public order order { get; set; }
        public store store1 { get; set; }
        public customer customer1 { get; set; }

        //order table
        public int oid { get; set; }
        public int cust_id { get; set; }
        public int s_id { get; set; }
        public int d_id { get; set; }
        public int payid { get; set; }
        public string oayment_status { get; set; }
        public string delivery_type { get; set; }
        public int address_id { get; set; }
        public int status_id { get; set; }
        public string notes { get; set; }
        public string reason { get; set; }
        public string prefer_datetime { get; set; }
        public int offer_id { get; set; }
        public string payment_mode { get; set; }
        public double order_subtotal { get; set; }
        public double discount { get; set; }
        public double tax { get; set; }
        public double delivery_charge { get; set; }
        public double final_total { get; set; }
        public string order_date { get; set; }
        //store table
        public string store_name { get; set; }
        public string email { get; set; }
        public string phone_code { get; set; }
        public string phone { get; set; }
        public string contact_person { get; set; }
        public string vat { get; set; }
        public string image { get; set; }
        public string lang_id { get; set; }
        public string cur_id { get; set; }
        public string status { get; set; }
        public string reg_date { get; set; }
        //customer table

        public string first_name { get; set; }
        public string last_name { get; set; }
        public string cust_email { get; set; }
        public string password { get; set; }
        public string profile_image { get; set; }
        public string cust_address_id { get; set; }
        public string cust_phone_code { get; set; }
        public string cust_phone { get; set; }
        public string cust_cur_id { get; set; }
        public string cust_status { get; set; }
        public string cust_reg_date { get; set; }
    }
}