﻿using ShopAndDrop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopAndDrop.Models.Admin
{
    public class Drivers
    {
        public string reg_date { get; set; }
        public int d_id { get; set; }
        public int s_id { get; set; }
        public int cur_id { get; set; }
        public int lang_id { get; set; }
        public string d_fname { get; set; }
        public string d_lname { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string phone_code { get; set; }
        public string status { get; set; }
        public string password { get; set; }
        public string profile_image { get; set; }
        public int verify_email { get; set; }
        public int verify_phone { get; set; }
        public order order { get; set; }
        public store store1 { get; set; }
        public customer customer1 { get; set; }
        public driver_reg driver1 { get; set; }
    }
}