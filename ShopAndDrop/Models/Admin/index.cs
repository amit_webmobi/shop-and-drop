﻿using ShopAndDrop;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ShopAndDrop.Models.Admin
{
    public class index
    {
        public user user { get; set; }
        [Required]
        public string email { get; set; }
        [Required]
        public string password { get; set; }
        public string phone { get; set; }
    }
}