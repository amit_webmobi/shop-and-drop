﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopAndDrop.Models.Admin
{
    public class Users
    {
        public string created_at { get; set; }
        public int u_id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string status { get; set; }
        public string photo { get; set; }
        public order order { get; set; }
        public store store1 { get; set; }
        public customer customer1 { get; set; }
    }
}