﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ShopAndDrop.Models.Admin
{
    public class SessionModel
    {
        public user user { get; set; }
       
        public string email { get; set; }
        public string firstname { get; set; }
        public string password { get; set; }
        public string phone { get; set; }
    }
}