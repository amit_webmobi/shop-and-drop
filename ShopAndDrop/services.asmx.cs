﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using ShopAndDrop.Common;
using ShopAndDrop.Models.Admin;
using System.Web.Script.Serialization;
using System.Web.Mvc;
using System.Configuration;
using System.Text;

namespace ShopAndDrop
{
    /// <summary>
    /// Summary description for services
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class services : System.Web.Services.WebService
    {

        [WebMethod]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        //public void Getuserdata()
        //{
        //    int from = 0, recordcount = 10;
        //    string query = "[getuserdata]";
        //    DataSet ds = new DataSet();

        //    //SqlDataAdapter da = new SqlDataAdapter(cmd);
        //    // da.Fill(ds);
        //    SqlConnection con;
        //    List<string> status = new List<string>();
        //    List<user> userlist = new List<user>();
        //    using (con = commonfile.GetDBConnection())
        //    {

        //        using (SqlCommand cmd = new SqlCommand("select * from user", con))
        //        {
        //            cmd.CommandType = CommandType.Text;
        //           // cmd.Parameters.AddWithValue("@ProtocolId", Session["Protocolid"] != null ? Convert.ToInt16(Session["Protocolid"].ToString()) : Convert.ToInt16("0"));
        //            con.Open();
        //            SqlDataReader rdr = cmd.ExecuteReader();
        //            while (rdr.Read())
        //            {
        //                user user1 = new user();
        //                user1.u_id = Convert.ToInt32(rdr["u_id"]);
        //                user1.first_name = rdr["first_name"].ToString();
        //                user1.last_name = rdr["last_name"].ToString();
        //                user1.email = rdr["email"].ToString();
        //               user1.phone = rdr["phone"].ToString();

        //                userlist.Add(user1);
        //                //status.Add(string.Format("{0}:{1}", reader["counts"].ToString(), reader["Status"].ToString()));
        //            }
        //        }
        //    }
        //    JavaScriptSerializer js = new JavaScriptSerializer();
        //    Context.Response.Write(js.Serialize(userlist));
        //}
        public void getUser()
        {

            using (DataClasses1DataContext dc = new DataClasses1DataContext())
            {
                var data = dc.users.ToList();
                JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Write(js.Serialize(data));
                //return Json(new { data = data }, JsonRequestBehavior.AllowGet);
            }
        }
        [WebMethod]
        public static string GetChart()
        {
            SqlConnection con;
            // string constr = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;
            using (con = commonfile.GetDBConnection())
            {
                string query = string.Format("select  count(oid),final_total from orders");
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = query;
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = con;
                    con.Open();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        StringBuilder sb = new StringBuilder();
                        sb.Append("[");
                        while (sdr.Read())
                        {
                            sb.Append("{");
                            System.Threading.Thread.Sleep(50);
                            string color = String.Format("#{0:X6}", new Random().Next(0x1000000));
                            sb.Append(string.Format("text :'{0}', value:{1}, color: '{2}'", sdr[0], sdr[1], color));
                            sb.Append("},");
                        }
                        sb = sb.Remove(sb.Length - 1, 1);
                        sb.Append("]");
                        con.Close();
                        return sb.ToString();
                    }
                }
            }
        }
    }
}
