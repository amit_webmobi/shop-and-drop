﻿using Newtonsoft.Json;
using ShopAndDrop.Common;
using ShopAndDrop.Models.Admin;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;

namespace ShopAndDrop.Controllers.Admin
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult loadstorechart1()
        {
            string query = "[getstoredata]";
            DataSet ds = new DataSet();
            SqlConnection con;
            List<store> storelist = new List<store>();
            using (con = commonfile.GetDBConnection())
            {
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@action", "Storename");
                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        store store = new store();
                        store.name = rdr["name"].ToString();
                        store.s_id = Convert.ToInt32(rdr["total1"].ToString());
                        storelist.Add(store);
                    }
                }
            }

            return Json(new { data = storelist }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult loaddriverchart1()
        {
            string query = "[getdriverdata]";
            DataSet ds = new DataSet();
            SqlConnection con;
            List<driver_reg> driverlist = new List<driver_reg>();
            using (con = commonfile.GetDBConnection())
            {
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@action", "topdriver");
                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        driver_reg driver = new driver_reg();
                        driver.d_fname = rdr["name"].ToString();
                        driver.email = rdr["total1"].ToString();
                        driverlist.Add(driver);
                    }
                }
            }

            return Json(new { data = driverlist }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Home()
        {
            Home home = new Home();
            SqlConnection con;
            string query = "getdashboard";
            int corder=0,ccust=0,cdriver=0,cstore=0;
            using (con = commonfile.GetDBConnection())
            {
                //Count Orders
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@action", "orders");
                    con.Open();
                    corder = Convert.ToInt32(cmd.ExecuteScalar());
                    home.countorder = corder.ToString();
                    con.Close();
                }
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@action", "customer");
                    con.Open();
                    ccust = Convert.ToInt32(cmd.ExecuteScalar());
                    home.countcust = ccust.ToString();
                    con.Close();
                }
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@action", "driver");
                    con.Open();
                    cdriver = Convert.ToInt32(cmd.ExecuteScalar());
                    home.countdriver = cdriver.ToString();
                    con.Close();
                }
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@action", "store");
                    con.Open();
                    cstore = Convert.ToInt32(cmd.ExecuteScalar());
                    home.countstore = cstore.ToString();
                    con.Close();
                }

            }
            return View(home);
        }
        [HttpPost]
        public ActionResult Home(Home home)
        {
            SqlConnection con;
            string query = "getdashboard";
            int corder = 0, ccust = 0, cdriver = 0, cstore = 0;
            using (con = commonfile.GetDBConnection())
            {
                //Count Orders
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@action", "orders");
                    con.Open();
                    corder = Convert.ToInt32(cmd.ExecuteScalar());
                    home.countorder = corder.ToString();
                    con.Close();
                }
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@action", "customer");
                    con.Open();
                    ccust = Convert.ToInt32(cmd.ExecuteScalar());
                    home.countcust = ccust.ToString();
                    con.Close();
                }
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@action", "driver");
                    con.Open();
                    cdriver = Convert.ToInt32(cmd.ExecuteScalar());
                    home.countdriver = cdriver.ToString();
                    con.Close();
                }
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@action", "store");
                    con.Open();
                    cstore = Convert.ToInt32(cmd.ExecuteScalar());
                    home.countstore = cstore.ToString();
                    con.Close();
                }

            }
            return View(home);
        }
        //Earning chart
        [HttpPost]
        public JsonResult Earningchart()
        {
            double data = 0;
            List<object> iData = new List<object>();
            //Creating sample data  
            SqlConnection con;
            DataTable dt = new DataTable();
            int currentMonth = DateTime.Now.Month;
            int pmonth = currentMonth;
            int currentYear = DateTime.Now.Year;
            if (currentMonth == 1)
            {
                pmonth = 12;
                currentYear = currentYear - 1;
            }

            dt.Columns.Add("Month", System.Type.GetType("System.String"));
            dt.Columns.Add("Earning", System.Type.GetType("System.Int32"));
            DataRow dr = dt.NewRow();

            for (int i = 1; i <= 12; i++)
            {

                using (con = commonfile.GetDBConnection())
                {
                    string query = "chartdata";
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@month", Convert.ToInt32(pmonth));
                        cmd.Parameters.AddWithValue("@year", Convert.ToInt32(currentYear));
                        cmd.Parameters.AddWithValue("@action", "Earning");
                        con.Open();
                        dr = dt.NewRow();
                        using (SqlDataReader sdr = cmd.ExecuteReader())
                        {
                            if (sdr.HasRows)
                            {

                                while (sdr.Read())
                                {
                                    if (sdr["avg_total"] != DBNull.Value)
                                    {


                                        data = Convert.ToDouble(sdr["avg_total"]);
                                        if (pmonth == 1)
                                        {
                                            dr["Month"] = "January";
                                            dr["Earning"] = data;
                                            dt.Rows.Add(dr);
                                        }
                                        else if (pmonth == 2)
                                        {
                                            dr["Month"] = "February";
                                            dr["Earning"] = data;
                                            dt.Rows.Add(dr);
                                        }
                                        else if (pmonth == 3)
                                        {
                                            dr["Month"] = "March";
                                            dr["Earning"] = data;
                                            dt.Rows.Add(dr);
                                        }
                                        else if (pmonth == 4)
                                        {
                                            dr["Month"] = "April";
                                            dr["Earning"] = data;
                                            dt.Rows.Add(dr);
                                        }
                                        else if (pmonth == 5)
                                        {
                                            dr["Month"] = "May";
                                            dr["Earning"] = data;
                                            dt.Rows.Add(dr);
                                        }
                                        else if (pmonth == 6)
                                        {
                                            dr["Month"] = "Jun";
                                            dr["Earning"] = data;
                                            dt.Rows.Add(dr);
                                        }
                                        else if (pmonth == 7)
                                        {
                                            dr["Month"] = "July";
                                            dr["Earning"] = data;
                                            dt.Rows.Add(dr);
                                        }
                                        else if (pmonth == 8)
                                        {
                                            dr["Month"] = "August";
                                            dr["Earning"] = data;
                                            dt.Rows.Add(dr);
                                        }
                                        else if (pmonth == 9)
                                        {
                                            dr["Month"] = "September";
                                            dr["Earning"] = data;
                                            dt.Rows.Add(dr);
                                        }
                                        else if (pmonth == 10)
                                        {
                                            dr["Month"] = "Octomber";
                                            dr["Earning"] = data;
                                            dt.Rows.Add(dr);
                                        }
                                        else if (pmonth == 11)
                                        {
                                            dr["Month"] = "November";
                                            dr["Earning"] = data;
                                            dt.Rows.Add(dr);
                                        }
                                        else if (pmonth == 12)
                                        {
                                            dr["Month"] = "December";
                                            dr["Earning"] = data;
                                            dt.Rows.Add(dr);
                                        }
                                    }
                                    else
                                    {
                                        data = 0;
                                        if (pmonth == 1)
                                        {
                                            dr["Month"] = "January";
                                            dr["Earning"] = data;
                                            dt.Rows.Add(dr);
                                        }
                                        else if (pmonth == 2)
                                        {
                                            dr["Month"] = "February";
                                            dr["Earning"] = data;
                                            dt.Rows.Add(dr);
                                        }
                                        else if (pmonth == 3)
                                        {
                                            dr["Month"] = "March";
                                            dr["Earning"] = data;
                                            dt.Rows.Add(dr);
                                        }
                                        else if (pmonth == 4)
                                        {
                                            dr["Month"] = "April";
                                            dr["Earning"] = data;
                                            dt.Rows.Add(dr);
                                        }
                                        else if (pmonth == 5)
                                        {
                                            dr["Month"] = "May";
                                            dr["Earning"] = data;
                                            dt.Rows.Add(dr);
                                        }
                                        else if (pmonth == 6)
                                        {
                                            dr["Month"] = "Jun";
                                            dr["Earning"] = data;
                                            dt.Rows.Add(dr);
                                        }
                                        else if (pmonth == 7)
                                        {
                                            dr["Month"] = "July";
                                            dr["Earning"] = data;
                                            dt.Rows.Add(dr);
                                        }
                                        else if (pmonth == 8)
                                        {
                                            dr["Month"] = "August";
                                            dr["Earning"] = data;
                                            dt.Rows.Add(dr);
                                        }
                                        else if (pmonth == 9)
                                        {
                                            dr["Month"] = "September";
                                            dr["Earning"] = data;
                                            dt.Rows.Add(dr);
                                        }
                                        else if (pmonth == 10)
                                        {
                                            dr["Month"] = "Octomber";
                                            dr["Earning"] = data;
                                            dt.Rows.Add(dr);
                                        }
                                        else if (pmonth == 11)
                                        {
                                            dr["Month"] = "November";
                                            dr["Earning"] = data;
                                            dt.Rows.Add(dr);
                                        }
                                        else if (pmonth == 12)
                                        {
                                            dr["Month"] = "December";
                                            dr["Earning"] = data;
                                            dt.Rows.Add(dr);
                                        }
                                    }

                                }

                            }

                        }

                    }
                }
                if (pmonth == 1)
                {
                    pmonth = 12;
                    currentYear = currentYear - 1;
                }
                else
                {
                    pmonth = pmonth - 1;
                }

                con.Close();

            }


            //Looping and extracting each DataColumn to List<Object>  
            foreach (DataColumn dc in dt.Columns)
            {
                List<object> x = new List<object>();
                x = (from DataRow drr in dt.Rows select drr[dc.ColumnName]).ToList();
                iData.Add(x);
            }
            //Source data returned as JSON  
            return Json(iData, JsonRequestBehavior.AllowGet);
        }


        public JsonResult saleschart()
        {
            double data = 0;
            List<object> iData = new List<object>();
            //Creating sample data  
            SqlConnection con;
            DataTable dt = new DataTable();
            int currentMonth = DateTime.Now.Month;
            int pmonth = currentMonth;
            int currentYear = DateTime.Now.Year;
            if (currentMonth == 1)
            {
                pmonth = 12;
                currentYear = currentYear - 1;
            }

            dt.Columns.Add("Month", System.Type.GetType("System.String"));
            dt.Columns.Add("Sales", System.Type.GetType("System.Int32"));
            DataRow dr = dt.NewRow();

            for (int i = 1; i <= 12; i++)
            {

                using (con = commonfile.GetDBConnection())
                {
                    string query = "chartdata";
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@month", Convert.ToInt32(pmonth));
                        cmd.Parameters.AddWithValue("@year", Convert.ToInt32(currentYear));
                        cmd.Parameters.AddWithValue("@action", "Sales");
                        con.Open();
                        dr = dt.NewRow();
                        using (SqlDataReader sdr = cmd.ExecuteReader())
                        {
                            if (sdr.HasRows)
                            {

                                while (sdr.Read())
                                {
                                    if (sdr["avg_sales"] != DBNull.Value)
                                    {


                                        data = Convert.ToDouble(sdr["avg_sales"]);
                                        if (pmonth == 1)
                                        {
                                            dr["Month"] = "January";
                                            dr["Sales"] = data;
                                            dt.Rows.Add(dr);
                                        }
                                        else if (pmonth == 2)
                                        {
                                            dr["Month"] = "February";
                                            dr["Sales"] = data;
                                            dt.Rows.Add(dr);
                                        }
                                        else if (pmonth == 3)
                                        {
                                            dr["Month"] = "March";
                                            dr["Sales"] = data;
                                            dt.Rows.Add(dr);
                                        }
                                        else if (pmonth == 4)
                                        {
                                            dr["Month"] = "April";
                                            dr["Sales"] = data;
                                            dt.Rows.Add(dr);
                                        }
                                        else if (pmonth == 5)
                                        {
                                            dr["Month"] = "May";
                                            dr["Sales"] = data;
                                            dt.Rows.Add(dr);
                                        }
                                        else if (pmonth == 6)
                                        {
                                            dr["Month"] = "Jun";
                                            dr["Sales"] = data;
                                            dt.Rows.Add(dr);
                                        }
                                        else if (pmonth == 7)
                                        {
                                            dr["Month"] = "July";
                                            dr["Sales"] = data;
                                            dt.Rows.Add(dr);
                                        }
                                        else if (pmonth == 8)
                                        {
                                            dr["Month"] = "August";
                                            dr["Sales"] = data;
                                            dt.Rows.Add(dr);
                                        }
                                        else if (pmonth == 9)
                                        {
                                            dr["Month"] = "September";
                                            dr["Sales"] = data;
                                            dt.Rows.Add(dr);
                                        }
                                        else if (pmonth == 10)
                                        {
                                            dr["Month"] = "Octomber";
                                            dr["Sales"] = data;
                                            dt.Rows.Add(dr);
                                        }
                                        else if (pmonth == 11)
                                        {
                                            dr["Month"] = "November";
                                            dr["Sales"] = data;
                                            dt.Rows.Add(dr);
                                        }
                                        else if (pmonth == 12)
                                        {
                                            dr["Month"] = "December";
                                            dr["Sales"] = data;
                                            dt.Rows.Add(dr);
                                        }
                                    }
                                    else
                                    {
                                        data = 0;
                                        if (pmonth == 1)
                                        {
                                            dr["Month"] = "January";
                                            dr["Sales"] = data;
                                            dt.Rows.Add(dr);
                                        }
                                        else if (pmonth == 2)
                                        {
                                            dr["Month"] = "February";
                                            dr["Sales"] = data;
                                            dt.Rows.Add(dr);
                                        }
                                        else if (pmonth == 3)
                                        {
                                            dr["Month"] = "March";
                                            dr["Sales"] = data;
                                            dt.Rows.Add(dr);
                                        }
                                        else if (pmonth == 4)
                                        {
                                            dr["Month"] = "April";
                                            dr["Sales"] = data;
                                            dt.Rows.Add(dr);
                                        }
                                        else if (pmonth == 5)
                                        {
                                            dr["Month"] = "May";
                                            dr["Sales"] = data;
                                            dt.Rows.Add(dr);
                                        }
                                        else if (pmonth == 6)
                                        {
                                            dr["Month"] = "Jun";
                                            dr["Sales"] = data;
                                            dt.Rows.Add(dr);
                                        }
                                        else if (pmonth == 7)
                                        {
                                            dr["Month"] = "July";
                                            dr["Sales"] = data;
                                            dt.Rows.Add(dr);
                                        }
                                        else if (pmonth == 8)
                                        {
                                            dr["Month"] = "August";
                                            dr["Sales"] = data;
                                            dt.Rows.Add(dr);
                                        }
                                        else if (pmonth == 9)
                                        {
                                            dr["Month"] = "September";
                                            dr["Sales"] = data;
                                            dt.Rows.Add(dr);
                                        }
                                        else if (pmonth == 10)
                                        {
                                            dr["Month"] = "Octomber";
                                            dr["Sales"] = data;
                                            dt.Rows.Add(dr);
                                        }
                                        else if (pmonth == 11)
                                        {
                                            dr["Month"] = "November";
                                            dr["Sales"] = data;
                                            dt.Rows.Add(dr);
                                        }
                                        else if (pmonth == 12)
                                        {
                                            dr["Month"] = "December";
                                            dr["Sales"] = data;
                                            dt.Rows.Add(dr);
                                        }
                                    }

                                }

                            }

                        }

                    }
                }
                if (pmonth == 1)
                {
                    pmonth = 12;
                    currentYear = currentYear - 1;
                }
                else
                {
                    pmonth = pmonth - 1;
                }

                con.Close();

            }


            //Looping and extracting each DataColumn to List<Object>  
            foreach (DataColumn dc in dt.Columns)
            {
                List<object> x = new List<object>();
                x = (from DataRow drr in dt.Rows select drr[dc.ColumnName]).ToList();
                iData.Add(x);
            }
            //Source data returned as JSON  
            return Json(iData, JsonRequestBehavior.AllowGet);
        }



        //public JsonResult loadstorechart()
        //{
        //    SqlConnection con2;
        //    int ocount = 0,other=0;
        //    using (con2 = commonfile.GetDBConnection())
        //    {
        //        using (SqlCommand cmd2 = new SqlCommand("select count(*) from orders", con2))
        //        {
        //            con2.Open();
        //            ocount = Convert.ToInt32(cmd2.ExecuteScalar());
        //            con2.Close();
        //        }
        //    }
        //    double data = 0;
        //    List<object> iData = new List<object>();
        //    //Creating sample data  
        //    SqlConnection con, con1;
        //    DataTable dt = new DataTable();


        //    dt.Columns.Add("name", System.Type.GetType("System.String"));
        //    dt.Columns.Add("stcount", System.Type.GetType("System.Int32"));
        //    DataRow dr = dt.NewRow();

        //    // for (int i = 1; i <= 12; i++)
        //    // {

        //    using (con = commonfile.GetDBConnection())
        //    {
        //        string query = "getstoredata";
        //        using (SqlCommand cmd = new SqlCommand(query, con))
        //        {
        //            cmd.CommandType = CommandType.StoredProcedure;
        //            cmd.Parameters.AddWithValue("@action", "Storecount");
        //            con.Open();
        //            dr = dt.NewRow();
        //            using (SqlDataReader sdr = cmd.ExecuteReader())
        //            {
        //                if (sdr.HasRows)
        //                {

        //                    while (sdr.Read())
        //                    {

        //                        if (sdr["s_id"] != DBNull.Value)
        //                        {
        //                            data = Convert.ToDouble(sdr["s_id"]);
        //                            int sid = Convert.ToInt32(sdr["sid"]);
        //                            other = other + sid;
        //                            using (con1 = commonfile.GetDBConnection())
        //                            {
        //                                string query1 = "select name from store where s_id='" + data + "'";
        //                                SqlCommand cmd1 = new SqlCommand(query1, con1);
        //                                cmd1.CommandType = CommandType.Text;
        //                                //cmd1.Parameters.AddWithValue("@action", "Storename");
        //                                con1.Open();
        //                                string name = cmd1.ExecuteScalar().ToString();
        //                                int data1 = ((sid * 100) / ocount);
        //                                dr["name"] = name;
        //                                dr["stcount"] = data1;
        //                                con1.Close();
        //                            }
        //                        }
        //                        dt.Rows.Add(dr.ItemArray);
        //                    }

        //                }

        //            }

        //        }
        //        // }

        //        con.Close();
        //        dr = dt.NewRow();
        //        dr["name"] = "others";
        //        int to = ocount - other;
        //        dr["stcount"] = ((to * 100) / ocount);
        //        dt.Rows.Add(dr);
        //    }


        //    //Looping and extracting each DataColumn to List<Object>  
        //    foreach (DataColumn dc in dt.Columns)
        //    {
        //        List<object> x = new List<object>();
        //        x = (from DataRow drr in dt.Rows select drr[dc.ColumnName]).ToList();
        //        iData.Add(x);
        //    }
        //    //Source data returned as JSON  
        //    return Json(iData, JsonRequestBehavior.AllowGet);

        //}
        //public JsonResult loaddriverchart()
        //{
        //    SqlConnection con2;
        //    int ocount = 0,other=0;
        //    using (con2 = commonfile.GetDBConnection())
        //    {
        //        using (SqlCommand cmd2 = new SqlCommand("select count(*) from orders", con2))
        //        {
        //            con2.Open();
        //            ocount = Convert.ToInt32(cmd2.ExecuteScalar());
        //            con2.Close();
        //        }
        //    }
        //    double data = 0;
        //    List<object> iData = new List<object>();
        //    //Creating sample data  
        //    SqlConnection con, con1;
        //    DataTable dt = new DataTable();


        //    dt.Columns.Add("name", System.Type.GetType("System.String"));
        //    dt.Columns.Add("drcount", System.Type.GetType("System.Int32"));
        //    DataRow dr = dt.NewRow();

        //    // for (int i = 1; i <= 12; i++)
        //    // {

        //    using (con = commonfile.GetDBConnection())
        //    {
        //        string query = "getdriverdata";
        //        using (SqlCommand cmd = new SqlCommand(query, con))
        //        {
        //            cmd.CommandType = CommandType.StoredProcedure;
        //            cmd.Parameters.AddWithValue("@action", "drivercount");
        //            con.Open();
        //            dr = dt.NewRow();
        //            using (SqlDataReader sdr = cmd.ExecuteReader())
        //            {
        //                if (sdr.HasRows)
        //                {

        //                    while (sdr.Read())
        //                    {

        //                        if (sdr["d_id"] != DBNull.Value)
        //                        {
        //                            data = Convert.ToDouble(sdr["d_id"]);
        //                            int sid = Convert.ToInt32(sdr["did"]);
        //                            other = other + sid;
        //                            using (con1 = commonfile.GetDBConnection())
        //                            {
        //                                string query1 = "select d_fname from driver_reg where d_id='" + data + "'";
        //                                SqlCommand cmd1 = new SqlCommand(query1, con1);
        //                                cmd1.CommandType = CommandType.Text;
        //                                //cmd1.Parameters.AddWithValue("@action", "Storename");
        //                                con1.Open();
        //                                string name = cmd1.ExecuteScalar().ToString();
        //                                int data1 = ((sid * 100) / ocount);
        //                                dr["name"] = name;
        //                                dr["drcount"] = data1;

        //                                con1.Close();
        //                            }
        //                        }
        //                        dt.Rows.Add(dr.ItemArray);
        //                    }

        //                }

        //            }

        //        }
        //        // }

        //        con.Close();

        //    }

        //    dr = dt.NewRow();
        //    dr["name"] = "others";
        //    int to = ocount - other;
        //    dr["drcount"] = ((to * 100) / ocount);
        //    dt.Rows.Add(dr);
        //    //Looping and extracting each DataColumn to List<Object>  
        //    foreach (DataColumn dc in dt.Columns)
        //    {
        //        List<object> x = new List<object>();
        //        x = (from DataRow drr in dt.Rows select drr[dc.ColumnName]).ToList();
        //        iData.Add(x);
        //    }
        //    //Source data returned as JSON  
        //    return Json(iData, JsonRequestBehavior.AllowGet);

        //}
        public JsonResult loadstatuschart()
        {
            SqlConnection con2;
            int ocount = 0;
            using (con2 = commonfile.GetDBConnection())
            {
                using (SqlCommand cmd2 = new SqlCommand("select count(*) from status", con2))
                {
                    con2.Open();
                    ocount = Convert.ToInt32(cmd2.ExecuteScalar());
                    con2.Close();
                }
            }
            double data = 0;
            List<object> iData = new List<object>();
            //Creating sample data  
            SqlConnection con, con1;
            DataTable dt = new DataTable();


            dt.Columns.Add("name", System.Type.GetType("System.String"));
            dt.Columns.Add("statuscount", System.Type.GetType("System.Int32"));
            DataRow dr = dt.NewRow();

            // for (int i = 1; i <= 12; i++)
            // {

            using (con = commonfile.GetDBConnection())
            {
                string query = "getdriverdata";
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@action", "statuscount");
                    con.Open();
                    dr = dt.NewRow();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        if (sdr.HasRows)
                        {

                            while (sdr.Read())
                            {

                                if (sdr["status_id"] != DBNull.Value)
                                {
                                    data = Convert.ToDouble(sdr["status_id"]);
                                    int sid = Convert.ToInt32(sdr["sid"]);
                                    using (con1 = commonfile.GetDBConnection())
                                    {
                                        string query1 = "select status_name from status where status_id='" + data + "'";
                                        SqlCommand cmd1 = new SqlCommand(query1, con1);
                                        cmd1.CommandType = CommandType.Text;
                                        //cmd1.Parameters.AddWithValue("@action", "Storename");
                                        con1.Open();
                                        string name = cmd1.ExecuteScalar().ToString();
                                        int data1 = ((sid * 100) / ocount);
                                        dr["name"] = name;
                                        dr["statuscount"] = data1;

                                        con1.Close();
                                    }
                                }
                                dt.Rows.Add(dr.ItemArray);
                            }

                        }

                    }

                }
                // }

                con.Close();

            }


            //Looping and extracting each DataColumn to List<Object>  
            foreach (DataColumn dc in dt.Columns)
            {
                List<object> x = new List<object>();
                x = (from DataRow drr in dt.Rows select drr[dc.ColumnName]).ToList();
                iData.Add(x);
            }
            //Source data returned as JSON  
            return Json(iData, JsonRequestBehavior.AllowGet);

        }

    }
}