﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ShopAndDrop.Models;
using ShopAndDrop.Models.Admin;
using ShopAndDrop;
using System.Dynamic;
using System.Configuration;
using System.Data.SqlClient;
using ShopAndDrop.Common;
using System.Data;

namespace ShopAndDrop.Controllers.Admin
{
    public class OrdersController : Controller
    {
        // GET: Orders
        DataClasses1DataContext db = new DataClasses1DataContext();
        public ActionResult getorder()
        {
            string query = "[getorderdata]";
            DataSet ds = new DataSet();
            SqlConnection con;
            List<Orders> orderlist = new List<Orders>();
            using (con = commonfile.GetDBConnection())
            {
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        Orders order = new Orders();
                        order.oid = Convert.ToInt32(rdr["oid"]);
                        order.s_id = Convert.ToInt32(rdr["s_id"]);
                        order.order_subtotal = Convert.ToDouble(rdr["order_subtotal"]);
                        order.final_total = Convert.ToDouble(rdr["final_total"]);
                        order.store_name = rdr["name"].ToString();
                        order.email = rdr[0].ToString();
                        order.cust_email = rdr[1].ToString();
                        order.first_name = rdr["first_name"].ToString();
                        order.last_name = rdr["last_name"].ToString();
                        orderlist.Add(order);
                    }
                }
            }

            return Json(new { data = orderlist }, JsonRequestBehavior.AllowGet);

        }
      
        public ActionResult Orders()
        {

            //List<customer> cust = db.customers.ToList();
            //List<store> st = db.stores.ToList();
            //List<order> ord = db.orders.ToList();
            //                     var query = from o in ord
            //                     join c in cust on o.cust_id equals c.cust_id into table1
            //                     from d in table1.ToList()
            //                     join s in st on o.s_id equals s.s_id into table2
            //                     from i in table2.ToList()
            //                     select new Orders
            //                     {
            //                        order =o,
            //                         store1 = i,
            //                         customer1 = d
            //                     };
            //return View(query.ToList());
            return View();
        }
        [HttpPost]
        public ActionResult Orders(Orders o)
        {
            return View();
        }
        //private static List<store> GetEmployees()
        //{
        //    List<store> employees = new List<store>();
        //    string query = ;
        //    string constr = ConfigurationManager.ConnectionStrings["Constring"].ConnectionString;
        //    using (SqlConnection con = new SqlConnection(constr))
        //    {
        //        using (SqlCommand cmd = new SqlCommand(query))
        //        {
        //            cmd.Connection = con;
        //            con.Open();
        //            using (SqlDataReader sdr = cmd.ExecuteReader())
        //            {
        //                while (sdr.Read())
        //                {
        //                    employees.Add(new EmployeeModel
        //                    {
        //                        EmployeeId = sdr["EmployeeID"].ToString(),
        //                        EmployeeName = sdr["Name"].ToString(),
        //                        City = sdr["City"].ToString(),
        //                        Country = sdr["Country"].ToString()
        //                    });
        //                }
        //                con.Close();
        //                return employees;
        //            }
        //        }
        //    }
        //}

    }
}