﻿using ShopAndDrop.Models.Admin;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using ShopAndDrop.Common;

namespace ShopAndDrop.Controllers.Admin
{
    public class LoginController : Controller
    {
        commonfile cf = new commonfile();
        DataClasses1DataContext db = new DataClasses1DataContext();
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(index m)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    SessionModel sm = null;
                    SqlConnection con;
                    using (con = commonfile.GetDBConnection())
                    {
                        con.Open();
                        SqlCommand command = new SqlCommand();
                        command.Connection = con;
                        command.CommandText = "logincheck";
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@email", m.email);
                        command.Parameters.AddWithValue("@password", m.password);// cf.Decrypt1(m.password));
                        SqlDataReader dataReader = command.ExecuteReader();
                        if (dataReader.HasRows)
                        {
                            Session["email"] = m.email;
                            StringBuilder msg = new StringBuilder();
                            //msg = CommonFunctions.passstring("Login Successful");
                            //SessionModel myprofile = null;
                            //System.Web.HttpContext.Current.Session["email"] = myprofile.user.first_name;
                            return View("../Home/Home");

                        }
                        else
                        {
                            StringBuilder errorMessages = new StringBuilder();
                           // errorMessages = CommonFunctions.passstring("Invalid");
                        }
                        con.Close();
                    }
                    //return Ok(protocol);
                }
                catch (SqlException sqlex)
                {
                    StringBuilder errorMessages = new StringBuilder();
                   // errorMessages = CommonFunctions.SPErrorMessage(sqlex);
                    // return Content(HttpStatusCode.InternalServerError, sqlex.Message);
                }
                //var v = db.users.Where(x => x.email == m.email & x.password == m.password).FirstOrDefault();
                //if (v != null)
                //{
                //    Session["email"] = v.email;
                //    return RedirectToAction("../Home/Home");
                //}
            }
            return View();
        }
    }
}