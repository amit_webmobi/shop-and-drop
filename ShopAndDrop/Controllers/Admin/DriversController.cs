﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ShopAndDrop.Models.Admin;
using System.Data;
using System.Data.SqlClient;
using ShopAndDrop.Common;

namespace ShopAndDrop.Controllers.Admin
{
    public class DriversController : Controller
    {
        public ActionResult Drivers()
        {
            return View();
        }
        // GET: Drivers
        DataClasses1DataContext db = new DataClasses1DataContext();
        common_config cc = new common_config();
        public ActionResult getDrivers()
        {
            string dd = cc.dateformat;
            string query = "[getdriverdata]";
            DataSet ds = new DataSet();
            SqlConnection con;
            List<Drivers> driverlist = new List<Drivers>();
            using (con = commonfile.GetDBConnection())
            {
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@action", "driverdata");
                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        Drivers user1 = new Drivers();
                        user1.d_id = Convert.ToInt32(rdr["d_id"]);
                        user1.d_fname = rdr["d_fname"].ToString();
                        user1.d_lname = rdr["d_lname"].ToString();
                        user1.email = rdr["email"].ToString();
                        user1.phone = rdr["phone"].ToString();
                        user1.reg_date = Convert.ToDateTime(rdr["reg_date"]).ToShortDateString();
                        user1.profile_image = rdr["profile_image"].ToString();
                        driverlist.Add(user1);
                    }
                }
            }

            //using (DataClasses1DataContext dc = new DataClasses1DataContext())
            //{
            //    var data = dc.users.ToList();
            return Json(new { data = driverlist }, JsonRequestBehavior.AllowGet);
            //}
        }
        
        

        [HttpGet]
        public ActionResult editdriver()
        {
            return View();
        }

        [HttpPost]
        public ActionResult editdriver(Drivers driver)
        {
            return View();
        }
    }
}