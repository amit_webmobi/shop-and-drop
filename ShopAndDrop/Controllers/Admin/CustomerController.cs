﻿using ShopAndDrop.Common;
using ShopAndDrop.Models.Admin;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShopAndDrop.Controllers.Admin
{
    public class CustomerController : System.Web.Mvc.Controller
    {
        // GET: Customer
        // GET: Customers
        DataClasses1DataContext db = new DataClasses1DataContext();
       
        public ActionResult getcust()
        {
            string query = "[getcustdata]";
            DataSet ds = new DataSet();
            SqlConnection con;
            List<customer> custlist = new List<customer>();
            using (con = commonfile.GetDBConnection())
            {
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        customer cust = new customer();
                        cust.cust_id = Convert.ToInt32(rdr["cust_id"]);
                        cust.first_name = rdr["first_name"].ToString();
                        cust.last_name = rdr["last_name"].ToString();
                        cust.email = rdr["email"].ToString();
                        cust.address_id = Convert.ToInt32(rdr["address_id"]);
                        cust.phone = rdr["phone"].ToString();
                        cust.status = Convert.ToInt32(rdr["status"]);
                        cust.reg_date = Convert.ToDateTime(rdr["reg_date"]);
                        custlist.Add(cust);
                    }
                }
            }
            
            return Json(new { data = custlist }, JsonRequestBehavior.AllowGet);
           
        }
        public ActionResult Customers()
        {
            //List<customer> cust = db.customers.ToList();
            //List<store> st = db.stores.ToList();
            //List<order> ord = db.orders.ToList();
            //var query = from o in ord
            //            join c in cust on o.cust_id equals c.cust_id into table1
            //            from d in table1.ToList()
            //            join s in st on o.s_id equals s.s_id into table2
            //            from i in table2.ToList()
            //            select new Customers
            //            {
            //                order = o,
            //                store1 = i,
            //                customer1 = d
            //            };
            return View();// query.ToList());
        }
        [HttpPost]
        public ActionResult Customers(Customers m)
        {
            return View();
        }
    }
}