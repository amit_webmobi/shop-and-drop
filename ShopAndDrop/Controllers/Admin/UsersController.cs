﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ShopAndDrop.Models.Admin;
using System.Data;
using System.Data.SqlClient;
using ShopAndDrop.Common;

namespace ShopAndDrop.Controllers.Admin
{
    public class UsersController : System.Web.Mvc.Controller
    {
        
        // GET: Users
        DataClasses1DataContext db = new DataClasses1DataContext();
        common_config cc = new common_config();
         public ActionResult getUser()
        {
            string dd = cc.dateformat;
            string query = "[getuserdata]";
            DataSet ds = new DataSet();
            SqlConnection con;
            List<Users> userlist = new List<Users>();
            using (con = commonfile.GetDBConnection())
            {
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        Users user1 = new Users();
                        user1.u_id = Convert.ToInt32(rdr["u_id"]);
                        user1.first_name = rdr["first_name"].ToString();
                        user1.last_name = rdr["last_name"].ToString();
                        user1.email = rdr["email"].ToString();
                        user1.phone = rdr["phone"].ToString();
                        user1.created_at = Convert.ToDateTime(rdr["created_at"]).ToShortDateString();
                        user1.photo = rdr["photo"].ToString();
                        userlist.Add(user1);
                    }
                }
            }

            //using (DataClasses1DataContext dc = new DataClasses1DataContext())
            //{
            //    var data = dc.users.ToList();
             return Json(new { data = userlist }, JsonRequestBehavior.AllowGet);
            //}
        }
        public ActionResult Users()
        {
            
            return View();
        }
        public ActionResult editUser(int? u_id)
        {

            return View("Users/Users");
        }

        [HttpPost]
        public ActionResult editUser(Users u)
        {
            return View("Users/Users");
        }
    }
}